var Article = /** @class */ (function () {
    function Article(titre, contenu, image) {
        this.titre = titre;
        this.contenu = contenu;
        this.image = image;
    }
    Article.prototype.logObject = function () {
        console.log(this);
    };
    return Article;
}());
var article = new Article("CF 63 bat Montpellier", "Super match à la maison", "googleimage");
var Planet = /** @class */ (function () {
    function Planet(id, nom, nbResident, image, distanceTerre) {
        this.id = id;
        this.nom = nom;
        this.nbResident = nbResident;
        this.image = image;
        this.distanceTerre = distanceTerre;
    }
    Planet.prototype.displayInfo = function () {
        console.log("Bienvenue sur la planète" + this.nom + " vous êtes à " +
            this.distanceTerre + " km de la terre");
    };
    return Planet;
}());
var planet = new Planet(1, "Tattoine", 5, "image-tatooine.jpg", 2300);
var Model = /** @class */ (function () {
    function Model(id, label) {
        this.id = id;
        this.label = label;
    }
    return Model;
}());
var Vaisseau = /** @class */ (function () {
    function Vaisseau(id, name, isLightSpeed, nbPassenger, model) {
        this.id = id;
        this.name = name;
        this.isLightSpeed = isLightSpeed;
        this.nbPassenger = nbPassenger;
        this.model = model;
    }
    return Vaisseau;
}());
article.logObject();
planet.displayInfo();
var model = new Model(1, "Cargo");
var vaisseau = new Vaisseau(1, "Faucon", true, 6, model);
console.log(vaisseau);
console.log("J'affiche le model !");
console.log(vaisseau.model);
