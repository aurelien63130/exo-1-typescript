class Article {
    titre: string;
    contenu: string;
    image: string;

    constructor(titre: string, contenu: string, image: string) {
        this.titre = titre;
        this.contenu = contenu;
        this.image = image;
    }

    logObject() : void {
       console.log(this);
    }
}

let article = new Article("CF 63 bat Montpellier",
    "Super match à la maison", "googleimage");


class Planet {
    id: number;
    nom: string;
    nbResident: number;
    image: string;
    distanceTerre: number;

    constructor(id: number, nom: string,
                nbResident: number, image: string, distanceTerre: number) {
        this.id = id;
        this.nom = nom;
        this.nbResident = nbResident;
        this.image = image;
        this.distanceTerre = distanceTerre;
    }

    displayInfo(): void{
        console.log("Bienvenue sur la planète" + this.nom + " vous êtes à " +
        this.distanceTerre + " km de la terre");
    }
}

let planet  = new Planet(1, "Tattoine", 5, "image-tatooine.jpg",
    2300);


class Model {
    id: number;
    label: string;

    constructor(id: number, label: string) {
        this.id = id;
        this.label = label;
    }
}

class Vaisseau {
    id: number;
    name: string;
    isLightSpeed: boolean;
    nbPassenger: number;
    model: Model;


    constructor(id: number, name: string, isLightSpeed: boolean, nbPassenger: number, model: Model) {
        this.id = id;
        this.name = name;
        this.isLightSpeed = isLightSpeed;
        this.nbPassenger = nbPassenger;
        this.model = model;
    }
}


article.logObject();

planet.displayInfo();

let model = new Model(1, "Cargo");

let vaisseau = new Vaisseau(1, "Faucon", true, 6, model);

console.log(vaisseau);

console.log("J'affiche le model !");

console.log(vaisseau.model);
